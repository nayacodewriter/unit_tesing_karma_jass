import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
 
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

export class Data { id: number;
  name: string; 
}

@Injectable () 
    export class VideoService{
 private heroesUrl = 'http://localhost:3000/';  // URL to web api
 
constructor(private http: Http) { }

initializer(module : string, parameters : string):  Promise<any[]> {
return this.http.get(this.heroesUrl + module + '/' )
             .toPromise()
             .then(response => response.json().data as any[])
             .catch(this.handleError);
}

 
private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}
    
}
    