import { AppComponent }                     from './app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }                               from '@angular/platform-browser';
import { DebugElement }                     from '@angular/core';
      import { ComponentFixtureAutoDetect } from '@angular/core/testing';

describe('AppComponent', function () {
  let de: DebugElement;
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent ],
       providers: [
    { provide: ComponentFixtureAutoDetect, useValue: true }
  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('h1'));
  });

  it('should create component', () => expect(comp).toBeDefined() );
 it('should create component 12', () =>{ 
   let myVar = 2
 expect(myVar).toBe(myVar) });

  it('should have expected <h1> text', () => {
   comp.name = "Yomen"
   
    fixture.detectChanges();
    const h1 = de.nativeElement;
    expect(h1.innerText).toMatch(/yomen/i,
      '<h1> should say something about "Angular"');
  });
    

});
